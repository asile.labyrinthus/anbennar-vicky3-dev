﻿hobgoblin_statocracy_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	interest_group_ig_armed_forces_pop_attraction_mult = 0.25
	interest_group_ig_armed_forces_pol_str_mult = 0.1
	country_officers_pol_str_mult = 1
	country_soldiers_pol_str_mult = 1
}

dalaire_unite_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	country_prestige_add = 50
}

dal_unified_dalaire = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	country_prestige_mult = 0.2
}

gh_unified_dalaire = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	country_prestige_mult = 0.1
}
