﻿ig_petty_bourgeoisie = {
	color = hsv{ 0.65 0.54 0.52 }
	texture = "gfx/interface/icons/ig_icons/petty_bourgeoisie.dds"
	layer = "revolution_dynamic_petty_bourgeoisie"
	index = 5

	ideologies = {
		ideology_reactionary
		ideology_meritocratic
		ideology_patriotic
		ideology_pb_machine_servitude
	}
	
	character_ideologies = {
		ideology_moderate
		ideology_traditionalist
		ideology_jingoist_leader
		ideology_protectionist
		ideology_sovereignist_leader
	}

	enable = {
		always = yes
	}

	# Deprecated; use on_enable effect to assign traits instead
	traits = {
		ig_trait_treasury_bonds
		ig_trait_middle_managers
		ig_trait_xenophobia
	}
	
	on_enable = {
		ig:ig_petty_bourgeoisie = {
			if = {
				limit = {
					owner = { c:A01 ?= this}	# Anbennar dummy
				}
				set_ig_trait = ig_trait:ig_trait_xenophobia
				set_ig_trait = ig_trait:ig_trait_civil_service
				set_ig_trait = ig_trait:ig_trait_treasury_bonds
			}
			if = {
				limit = {
					owner = { c:E01 ?= this}
				}
				set_ig_trait = ig_trait:ig_trait_self_reliance
				set_ig_trait = ig_trait:ig_trait_civil_service
				set_ig_trait = ig_trait:ig_trait_treasury_bonds
			}
			else = {
				set_ig_trait = ig_trait:ig_trait_xenophobia
				set_ig_trait = ig_trait:ig_trait_middle_managers
				set_ig_trait = ig_trait:ig_trait_treasury_bonds
			}
		}
		if = {
			limit = {
				OR = {
					#All Aelantiri settlers (using cultures so that later formables or whatever are affected too)
					country_has_primary_culture = cu:concordian
					country_has_primary_culture = cu:zanlibi
					country_has_primary_culture = cu:towerfoot_halfling
					country_has_primary_culture = cu:valorborn
					country_has_primary_culture = cu:steadsman
					country_has_primary_culture = cu:horizon_elf
					country_has_primary_culture = cu:dustman
					country_has_primary_culture = cu:east_ynnsman
					country_has_primary_culture = cu:pipefoot_halfling
					country_has_primary_culture = cu:ynngarder_half_orc
					country_has_primary_culture = cu:steel_dwarf
					country_has_primary_culture = cu:themarian
					country_has_primary_culture = cu:alloy_dwarf
					country_has_primary_culture = cu:abtazari
					country_has_primary_culture = cu:ruinscourge_gnoll
					c:B03 ?= this
					c:B04 ?= this
					country_has_primary_culture = cu:neratican
					country_has_primary_culture = cu:sornicandi
					country_has_primary_culture = cu:glacier_gnome
					country_has_primary_culture = cu:soot_goblin
					country_has_primary_culture = cu:steelscale_kobold
					country_has_primary_culture = cu:tinker_gnome
					country_has_primary_culture = cu:dhanaenno
					country_has_primary_culture = cu:dawn_elf
					country_has_primary_culture = cu:kioha_harpy
					country_has_primary_culture = cu:ozgar_orc
				}
			}
			ig:ig_petty_bourgeoisie = {
				remove_ideology = ideology_reactionary
				add_ideology = ideology_nativist
			}
		}
		if = {
			limit = {
				c:E01 ?= this
			}
			ig:ig_petty_bourgeoisie = {
				set_interest_group_name = ig_lake_traders
				remove_ideology = ideology_reactionary
				remove_ideology = ideology_patriotic
				add_ideology = ideology_laissez_faire
				add_ideology = ideology_liberal
				add_ideology = ideology_triunic_traders
			}
		}
	}

	on_disable = {}
	on_character_ig_membership = {}

	pop_potential = {
		custom_tooltip = {
			text = would_be_accepted_under_national_supremacy_tt
			this.culture = {
				OR = {
					AND = {
						shares_heritage_trait_with_any_primary_culture = prev.owner
						shares_non_heritage_trait_with_any_primary_culture = prev.owner
					}
				}
			}
		}
		NOR = {
			is_pop_type = aristocrats
			is_pop_type = capitalists
		}
		OR = {
			pop_employment_building_group = bg_manufacturing
			pop_employment_building_group = bg_service
			pop_employment_building_group = bg_urban_facilities
			pop_employment_building_group = bg_government
			pop_employment_building_group = bg_logging
			pop_employment_building_group = bg_mining
			pop_employment_building_group = bg_fishing
			pop_employment_building_group = bg_whaling
			pop_employment_building_group = bg_oil_extraction
			AND = {
				custom_tooltip = {
					text = owner_has_homesteading_tt
					owner = {
						OR = {
							has_law = law_type:law_homesteading
							has_law = law_type:law_commercialized_agriculture
						}
					}
				}
				OR = {
					pop_employment_building_group = bg_agriculture
					pop_employment_building_group = bg_ranching
					pop_employment_building_group = bg_plantations
				}
				strata = middle
			}
		}
	}

	pop_weight = {
		value = 50

		add = {
			desc = "POP_ADVENTURERS"
			if = {
				limit = { 
					is_pop_type = adventurers
				}
				value = 25
			}
		}
		if = {
			limit = {
				strata = lower
			}
			add = {
				desc = "POP_BASE_ATTRACTION"
				value = 25
			}
		}
		else_if = {
			limit = {
				strata = middle
			}
			add = {
				desc = "POP_BASE_ATTRACTION"
				value = 50
			}
		}

		add = {
			desc = "POP_SHOPKEEPERS"
			if = {
				limit = {
					is_pop_type = shopkeepers
				}
				value = 250
			}
		}

		add = {
			desc = "POP_CLERKS"
			if = {
				limit = {
					is_pop_type = clerks
				}
				value = 100
			}
		}

		add = {
			desc = "POP_ENGINEERS"
			if = {
				limit = {
					is_pop_type = engineers
				}
				value = 50
			}
		}

		if = {
			limit = {
				owner = {
					has_law = law_type:law_elected_bureaucrats
				}
			}
			add = {
				desc = "POP_BUREAUCRATS_ELECTED"
				if = {
					limit = {
						is_pop_type = bureaucrats
					}
					value = 50
				}
			}
		}

		add = {
			desc = "HOMESTEADING_LAW"
			if = {
				limit = {
					is_pop_type = farmers
					owner = {
						has_law = law_type:law_homesteading
					}
				}
				value = 25

				if = {
					limit = {
						state = {
							is_slave_state = yes
						}
					}
					subtract = 25
				}
			}
		}

		add = {
			desc = "URBAN_POP"
			if = {
				limit = {
					NOR = {
						pop_employment_building_group = bg_agriculture
						pop_employment_building_group = bg_ranching
						pop_employment_building_group = bg_plantations
					}
				}
				value = 25
				add = this.standard_of_living
			}
		}

		multiply = {
			desc = "LEADER_POPULARITY"
			scope:interest_group = {
				leader ?= {
					value = popularity
					multiply = 0.0025
					add = 1
					max = 1.25
					min = 0.75
				}
			}
		}

		multiply = {
			desc = "decentralized_power"
			value = 1.0

			if = {
				limit = {
					owner = {
						is_country_type = decentralized
					}
				}
				value = 0.0
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = {
				desc = "IN_GOVERNMENT_ATTRACTION"
				value = 1
				add = scope:interest_group.modifier:interest_group_in_government_attraction_mult
				min = 0
			}
		}
		
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_LOYALISTS"
				value = pop_loyalist_fraction
				divide = 4
				add = 1
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = no
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_RADICALS"
				value = pop_radical_fraction
				divide = 4
				add = 1
			}
		}
	}

	monarch_weight = {
		value = 1
		# Monarch is not likely to adopt a marginal IG
		if = {
			limit = {
				is_marginal = yes
			}
			multiply = {
				value = 0.1
			}
		}
		# Monarch is more likely to adopt a Powerful IG
		if = {
			limit = {
				is_powerful = yes
			}
			multiply = {
				value = 2
			}
		}
		# More likely with Elected Bureaucrats
		if = {
			limit = {
				owner = {
					has_law = law_type:law_elected_bureaucrats
				}
			}
			multiply = {
				value = 1.5
			}
		}
		# More likely with ethnostate
		if = {
			limit = {
				owner = {
					has_law = law_type:law_ethnostate
				}
			}
			multiply = {
				value = 2
			}
		}
	}

	agitator_weight = {
		# Agitators more likely to come from populist IG's rather than elitist
		value = 1.0
		# Agitators will never be part of a marginalized IG
		if = {
			limit = {
				owner.ig:ig_petty_bourgeoisie = {
					is_marginal = yes
				}
			}
			multiply = {
				value = 0
			}
		}
	}

	commander_weight = {
		value = 1.0
	}

	noble_chance = {
		value = 0.1
	}

	female_commander_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.50
			}
		}
	}

	female_politician_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = { #variable from country history gets set after IGs being loaded, this need to be done for it to work
				owner = {
					OR = {
						c:F13 ?= this
					}
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.50
			}
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_womens_suffrage
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.025
			}
		}
	}

	female_agitator_chance = {
		value = 0.025
		#Anbennar
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.475
			}
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_own_property
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.025
			}
		}	
		
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_in_the_workplace
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.025
			}
		}

		if = {
			limit = {
				owner = {
					has_law = law_type:law_womens_suffrage
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.025
			}
		}		
	}

	# The chance that a commander belonging to this IG takes over leadership when it changes
	# scope:character is the most popular commander in the IG
	commander_leader_chance = {
		value = 0.25
		multiply = ig_commander_leader_chance_mult
	}
}
