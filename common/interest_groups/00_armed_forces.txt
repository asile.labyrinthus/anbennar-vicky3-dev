﻿ig_armed_forces = {
	color = hsv{ 0.09 0.29 0.39 }
	texture = "gfx/interface/icons/ig_icons/armed_forces.dds"
	layer = "revolution_dynamic_armed_forces"
	index = 0

	ideologies = {
		ideology_jingoist
		ideology_loyalist
		ideology_patriotic
	}
	
	character_ideologies = {
		ideology_moderate
		#ideology_orleanist
		#ideology_bonapartist
	}

	enable = {
		always = yes
	}

	# Deprecated; use on_enable effect to assign traits instead
	traits = {
		ig_trait_patriotic_fervor
		ig_trait_veteran_consultation
		ig_trait_materiel_waste
	}

	on_enable = {
		ig:ig_armed_forces = {
			# Anbennar
			if = {
				limit = {
					owner = {
						c:A01 ?= this
					}
				}
				set_ig_trait = ig_trait:ig_trait_materiel_waste
				set_ig_trait = ig_trait:ig_trait_veteran_consultation
				set_ig_trait = ig_trait:ig_trait_elan_vital
			}
			# Gnomes
			else_if = {
				limit = {
					owner = {
						OR = {
							country_has_primary_culture = cu:cliff_gnome
							country_has_primary_culture = cu:creek_gnome
							country_has_primary_culture = cu:glacier_gnome
						}
					}
				}
				set_ig_trait = ig_trait:ig_trait_materiel_waste
				set_ig_trait = ig_trait:ig_trait_veteran_consultation
				set_ig_trait = ig_trait:ig_trait_self_strengthening
			}
			# Ghankedhen
			else_if = {
				limit = {
					owner = {
						c:R02 ?= this
					}
				}
				set_ig_trait = ig_trait:ig_trait_materiel_waste
				set_ig_trait = ig_trait:ig_trait_self_supplied_arms
				set_ig_trait = ig_trait:ig_trait_self_strengthening
			}
			else = {
				set_ig_trait = ig_trait:ig_trait_materiel_waste
				set_ig_trait = ig_trait:ig_trait_veteran_consultation
				set_ig_trait = ig_trait:ig_trait_patriotic_fervor
			}
		}

		if = {
			limit = {
				OR = {
					country_has_primary_culture = cu:dustman
					country_has_primary_culture = cu:steadsman
					country_has_primary_culture = cu:horizon_elf
					country_has_primary_culture = cu:ynngarder_half_orc
				}
			}
			ig:ig_armed_forces = {
				add_ideology = ideology_rangerism
			}
		}

		### Specific
		# Order of St. Aldresia for Rectorate
		if = {
			limit = {
				c:A33 ?= this
			}
			ig:ig_armed_forces = {
				set_interest_group_name = ig_order_of_st_aldresia
				add_ideology = ideology_aldresian
			}
		}
		# Lake Fed
		else_if = {
			limit = {
				c:E01 ?= this
			}
			ig:ig_armed_forces = {
				set_interest_group_name = ig_lake_soldiers
				remove_ideology = ideology_loyalist
				add_ideology = ideology_triunic_soldiers
			}
		}
		# Jadd Empire
		else_if = {
			limit = {
				OR = {
					c:F01 ?= this
					c:R01 ?= this
				}
			}
			ig:ig_armed_forces = {
				set_interest_group_name = ig_phoenix_legions
			}
		}
		# Ghankedhen
		else_if = {
			limit = {
				c:R02 ?= this
			}
			ig:ig_armed_forces = {
				set_interest_group_name = ig_paravimata_rangers
				remove_ideology = ideology_jingoist
				add_ideology = ideology_elite_core
			}
		}
	}
	on_disable = {}
 
	#priority_cultures = { #Country scope  #anbennar commented out
	#	rule = {
	#		trigger = {
	#			c:BIC ?= this
	#			any_primary_culture = { 
	#				cu:british = this
	#			}
	#		}
	#		cultures = {
	#			british scottish irish
	#		}
	#	}
	#}

	on_character_ig_membership = {}
	
	pop_potential = {
		OR = {
			is_pop_type = soldiers
			is_pop_type = officers
			is_pop_type = aristocrats
			is_pop_type = adventurers #anbennar
			pop_acceptance >= acceptance_status_4
		}
	}

	pop_weight = {
		value = 0

		#Anbennar

		add = {
			desc = "POP_ADVENTURERS"	#represents the mercenary sellsword side of adventurers
			if = {
				limit = { 
					is_pop_type = adventurers
				}
				value = 25
			}
		}
		
		add = {
			desc = "POP_NOT_DISCRIMINATED"
			if = {
				limit = {
					pop_acceptance >= acceptance_status_4
				}
				value = 25
				if = { # Militarisation of colonial settlers
					limit = {
						owner = {
							has_law = law_type:law_colonial_resettlement
						}
						state = {
							is_incorporated = no
						}
					}
					add = 50
				}
				if = { # Separation of the military and people
					limit = {
						NOR = {
							is_pop_type = soldiers
							is_pop_type = officers
						}
						owner = {
							has_law = law_type:law_professional_army
							#Anbennar - Hobgoblins don't separate army and civilians
							NOT = {
								any_primary_culture = {
									has_discrimination_trait = hobgoblin_race_heritage
								}
							}
						}
					}
					divide = 2
				}
			}
		}

		add = {
			desc = "POP_SOLDIERS"
			if = {
				limit = {
					is_pop_type = soldiers
				}
				value = 100
				if = { # Institutional loyalty
					limit = {
						owner = {
							has_law = law_type:law_professional_army
						}
					}
					add = 50
				}
				if = { # Pay makes soldiers more inclined to be loyal to Armed Forces
					limit = {
						owner = {
							military_wage_level = very_low
						}
					}
					multiply = 0.75
				}
				if = {
					limit = {
						owner = {
							military_wage_level = low
						}
					}
					multiply = 0.85
				}
				if = {
					limit = {
						owner = {
							military_wage_level = medium
						}
					}
					multiply = 1
				}
				if = {
					limit = {
						owner = {
							military_wage_level = high
						}
					}
					multiply = 1.15
				}
				if = {
					limit = {
						owner = {
							military_wage_level = very_high
						}
					}
					multiply = 1.25
				}
			}
		}

		add = {
			desc = "POP_ARISTOCRATS"
			if = {
				limit = {
					is_pop_type = aristocrats
				}
				value = 10
				if = {
					limit = {
						owner = {
							has_law = law_type:law_peasant_levies
						}
					}
					add = 50
				}
				if = {
					limit = {
						owner = {
							has_law = law_type:law_stratocracy
						}
					}
					add = 50
				}
			}
		}

		add = {
			desc = "POP_OFFICERS"
			if = {
				limit = {
					is_pop_type = officers
				}
				value = 250
			}
		}

		add = { # There's one firearm for every twelve people on the planet. The question is, how do we arm the other eleven?
			desc = "MILITARY_INDUSTRIES"
			if = {
				limit = {
					OR = {
						pop_employment_building_group = bg_munition_plants
						pop_employment_building_group = bg_arms_industries
						pop_employment_building_group = bg_artillery_foundaries
						pop_employment_building_group = bg_military_shipyards
					}
				}
				if = {
					limit = {
						strata = middle
					}
					value = 75
				}
				if = {
					limit = {
						strata = upper
					}
					value = 150
				}
			}
		}
		multiply = {
			desc = "LEADER_POPULARITY"
			scope:interest_group = {
				leader ?= {
					value = popularity
					multiply = 0.0025
					add = 1
					max = 1.25
					min = 0.75
				}
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = {
				desc = "IN_GOVERNMENT_ATTRACTION"
				value = 1
				add = scope:interest_group.modifier:interest_group_in_government_attraction_mult
				min = 0
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_LOYALISTS"
				value = pop_loyalist_fraction
				divide = 4
				add = 1
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = no
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_RADICALS"
				value = pop_radical_fraction
				divide = 4
				add = 1
			}
		}
	}

	monarch_weight = {
		value = 1
		# Monarch is not likely to adopt a marginal IG
		if = {
			limit = {
				is_marginal = yes
			}
			multiply = {
				value = 0.1
			}
		}
		# Monarch is more likely to adopt a Powerful IG
		if = {
			limit = {
				is_powerful = yes
			}
			multiply = {
				value = 2
			}
		}
		# Monarch more likely to be Armed Forces under an autocratic or oligarchic regime
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_oligarchy
						has_law = law_type:law_autocracy
						has_law = law_type:law_single_party_state
					}
				}
			}
			multiply = {
				value = 2
			}
		}
		# Monarch is less likely to be Armed forces in a democratic monarchy
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_universal_suffrage
						has_law = law_type:law_census_voting
					}
				}
			}
			multiply = {
				value = 0.5
			}
		}
		# Less likely without Professional Army or Mass Conscription
		if = {
			limit = {
				owner = {
					NOR = {
						has_law = law_type:law_professional_army
						has_law = law_type:law_mass_conscription
					}
				}
			}
			multiply = {
				value = 0.5
			}
		}
	}

	agitator_weight = {
		# Agitators more likely to come from populist IG's rather than elitist
		value = 0.5
		# Agitators will never be part of a marginalized IG
		if = {
			limit = {
				owner.ig:ig_armed_forces = {
					is_marginal = yes
				}
			}
			multiply = {
				value = 0
			}
		}
	}

	commander_weight = {
		value = 5.0
	}

	noble_chance = {
		value = 0.5
	}

	female_commander_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.50
			}
		}
	}

	female_politician_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = {
				owner = { #variable from country history gets set after IGs being loaded, this need to be done for it to work
					OR = {
						c:F13 ?= this
					}
					NOT = { has_law = law_type:law_womens_suffrage }
				}

			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}

			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}

			}
			add = {
				value = 0.50
			}
		}
	}

	female_agitator_chance = {
		value = 0.01
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.90
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.49
			}
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_own_property
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}	
		
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_in_the_workplace
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}

		if = {
			limit = {
				owner = {
					has_law = law_type:law_womens_suffrage
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}		
	}

	# The chance that a commander belonging to this IG takes over leadership when it changes
	# scope:character is the most popular commander in the IG
	commander_leader_chance = {
		value = 0.75
		multiply = ig_commander_leader_chance_mult
	}
}