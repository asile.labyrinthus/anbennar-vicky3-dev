﻿pmg_acquisition_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_market_trading
		pm_mage_gardens
		pm_growth_labs
		pm_synthetic_monster_parts
	}
}

pmg_treatments_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_patent_stills_magical_reagents
		pm_essence_extraction
	}
}

pmg_storage_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_basic_storage
		pm_porcelain_pills
		pm_refrigeration_magical_reagents
		pm_plastic_packaging
	}
}

pmg_base_building_skyship_yards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sky_galleons_building_skyship_yards
        pm_steel_skyships_building_skyship_yards
		pm_arc_welded_skyships_building_skyship_yards
	}
}

pmg_military_base_building_skyship_yards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_civilian_shipbuilding_building_skyship_yards
        pm_military_shipbuilding_skyship_yards
		pm_extensive_military_shipbuilding_building_skyship_yards
	}
}

pmg_base_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_basic_liquefaction_building_damesoil_refinery
		pm_reagent_liquefaction_building_damesoil_refinery
		pm_doodad_liquefaction_building_damesoil_refinery
	}
}

pmg_storage_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_storage_building_damesoil_refinery
		pm_porcelain_storage_building_damesoil_refinery
		pm_steel_storage_building_damesoil_refinery
	}
}

pmg_transmutation_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_transmutation_building_damesoil_refinery
		pm_coal_transmutation_building_damesoil_refinery
	}
}

pmg_collection_procedure_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_syringes_building_blueblood_plants
		pm_arcane_leeches_building_blueblood_plants
		pm_full_transfusion_building_blueblood_plants
		pm_mechanical_extractors_building_blueblood_plants
		pm_blood_farms_building_blueblood_plants
	}
}

pmg_patient_acquisition_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_donation_centres_building_blueblood_plants
		pm_penal_patients_building_blueblood_plants
		pm_unwilling_subjects_building_blueblood_plants
		pm_industrial_scale_acquisition_building_blueblood_plants
	}
}

pmg_refrigeration_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_unrefrigerated_building_blueblood_plants
		pm_frost_magic_refrigeration_building_blueblood_plants
		pm_refrigerated_storage_building_blueblood_plants
		pm_chemical_refrigeration_building_blueblood_plants
	}
}

pmg_power_supply_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_crystal_socketry_building_doodad_manufacturies
		pm_damesoil_tubes_building_doodad_manufacturies
		pm_damestear_core_building_doodad_manufacturies
	}
}

pmg_casing_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_artisan_artificery_building_doodad_manufacturies
		pm_tearite_casings_building_doodad_manufacturies
		pm_relics_casings_building_doodad_manufacturies
		pm_porcelain_stabilized_tearite_casings_building_doodad_manufacturies
		pm_mimic_precursor_steel_casings_building_doodad_manufacturies
	}
}

pmg_wiring_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_copper_wires_building_doodad_manufacturies
		pm_arcane_copper_wires_building_doodad_manufacturies
		pm_dagrite_autowires_building_doodad_manufacturies
	}
}

pmg_automation_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_automation_disabled_building_doodad_manufacturies
		pm_rotary_valve_engine_building_doodad_manufacturies
		pm_assembly_lines_building_doodad_manufacturies
		pm_automata_laborers_doodad_manufacturies
		pm_automata_machinists_doodad_manufacturies
	}
}

pmg_control_method_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_natural_production
		pm_control_chip_v2
		pm_control_chip_v1
	}
}

pmg_power_source_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_refurbished_insyaan_cores
		pm_refurbished_insyaan_cores_freedom
		#pm_bound_elemental
		pm_damesoil_engine_automatories
		pm_damestear_core_automatories
		pm_damesoil_engine_automatories_freedom
		pm_damestear_core_automatories_freedom
	}
}

pmg_frame_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_steelforged_frame
		pm_porcelain_shell
		pm_synthetic_body
		pm_new_insyaan_frame
		pm_steelforged_frame_freedom
		pm_porcelain_shell_freedom
		pm_synthetic_body_freedom
		pm_new_insyaan_frame_freedom
	}
}

pmg_automation_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation
		pm_self_repairing_automata_automatories
		pm_self_building_automata_automatories
	}
}
