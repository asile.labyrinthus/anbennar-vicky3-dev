﻿TRADE_ROUTES = {
	# Anbennar
	c:A01={
		create_trade_route = {
			goods = wood
			level = 8
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = dye
			level = 2
			direction = import
			target = c:A09.market
		}
		create_trade_route = {
			goods = manowars
			level = 4
			direction = import
			target = c:C01.market
		}
		create_trade_route = {
			goods = manowars
			level = 3
			direction = import
			target = c:C03.market
		}
	}
	
	# Vivin Empire
	c:A02 ?= {
		create_trade_route = {
			goods = wood
			level = 3
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = wine
			level = 3
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = dye
			level = 1
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = fabric
			level = 5
			direction = import
			target = c:B98.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 8
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 2
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = sugar
			level = 9
			direction = import
			target = c:A09.market
		}
		create_trade_route = {
			goods = magical_reagents
			level = 3
			direction = import
			target = c:A04.market
		}
		create_trade_route = {
			goods = meat
			level = 2
			direction = export
			target = c:A03.market
		}
		create_trade_route = {
			goods = glass
			level = 1
			direction = export
			target = c:A03.market
		}
	}
	
	#Lorent
	c:A03 ?= {
		create_trade_route = {
			goods = wood
			level = 13
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = hardwood
			level = 8
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = fabric
			level = 4
			direction = import
			target = c:B98.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 2
			direction = export
			target = c:B98.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 3
			direction = export
			target = c:F01.market
		}
	}
	
	# Northern League
	c:A04 ?= {
		create_trade_route = {
			goods = dye
			level = 9
			direction = import
			target = c:A09.market
		}
		create_trade_route = {
			goods = sugar
			level = 14
			direction = import
			target = c:A09.market
		}
		create_trade_route = {
			goods = silk
			level = 5
			direction = import
			target = c:F01.market
		}
		create_trade_route = {
			goods = fabric
			level = 18
			direction = import
			target = c:F01.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 2
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = clothes
			level = 7
			direction = export
			target = c:A10.market
		}
		create_trade_route = {
			goods = magical_reagents
			level = 7
			direction = export
			target = c:A03.market
		}
	}
	
	#Gnomish Hierarchy
	c:A06 ?= {
		create_trade_route = {
			goods = wood
			level = 3
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = paper
			level = 6
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = damestear
			level = 1
			direction = import
			target = c:C02.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 3
			direction = import
			target = c:A03.market
		}
	}
	
	# Grombar
	c:A10 ?= {
		create_trade_route = {
			goods = dye
			level = 5
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = grain
			level = 3
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = clippers
			level = 3
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = sugar
			level = 14
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = wine
			level = 5
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = liquor
			level = 5
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = tea
			level = 10
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 3
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = clothes
			level = 3
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = meat
			level = 2
			direction = export
			target = c:A03.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 6
			direction = export
			target = c:A06.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 3
			direction = export
			target = c:A03.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 5
			direction = export
			target = c:A09.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = export
			target = c:C02.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = export
			target = c:E02.market
		}
		create_trade_route = {
			goods = porcelain
			level = 1
			direction = export
			target = c:E02.market
		}
	}
	
	#Small Country
	c:A14 ?= {
		create_trade_route = {
			goods = clippers
			level = 4
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = manowars
			level = 1
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = grain
			level = 3
			direction = export
			target = c:A06.market
		}
		create_trade_route = {
			goods = tools
			level = 5
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = tools
			level = 5
			direction = import
			target = c:A03.market
		}
		create_trade_route = {
			goods = fertilizer
			level = 2
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = paper
			level = 2
			direction = import
			target = c:A03.market
		}
	}
	
	#Ancardia
	c:A22 ?= {
		create_trade_route = {
			goods = wine
			level = 2
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = dye
			level = 1
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = hardwood
			level = 2
			direction = import
			target = c:A01.market
		}
		# For iron mining + logging
		create_trade_route = {
			goods = tools
			level = 3
			direction = import
			target = c:A04.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = import
			target = c:A04.market
		}
		create_trade_route = {
			goods = iron
			level = 4
			direction = import
			target = c:A25.market
		}
		create_trade_route = {
			goods = paper
			level = 3
			direction = import
			target = c:A25.market
		}
	}
	
	# Araionn
	c:A25 ?= {
		create_trade_route = {
			goods = small_arms
			level = 2
			direction = import
			target = c:A22.market
		}
		create_trade_route = {
			goods = grain
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = tools
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = fabric
			level = 3
			direction = import
			target = c:A27.market
		}
	}
	
	# Wyvernheart
	c:A26 ?= {
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = import
			target = c:A04.market
		}
		create_trade_route = {
			goods = small_arms
			level = 3
			direction = import
			target = c:A22.market
		}
		create_trade_route = {
			goods = paper
			level = 1
			direction = import
			target = c:A30.market
		}
		create_trade_route = {
			goods = wood
			level = 1
			direction = export
			target = c:A04.market
		}
		create_trade_route = {
			goods = wood
			level = 1
			direction = export
			target = c:A22.market
		}
		create_trade_route = {
			goods = wood
			level = 2
			direction = export
			target = c:A30.market
		}
	}
	
	# Blademarches
	c:A27 ?= {
		create_trade_route = {
			goods = artillery
			level = 2
			direction = import
			target = c:A02.market
		}
		create_trade_route = {
			goods = wine
			level = 3
			direction = import
			target = c:A28.market
		}
	}
	
	#Rosande
	c:A28={
		create_trade_route = {
			goods = grain
			level = 5
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = tools
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = liquor
			level = 4
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = small_arms
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = furniture
			level = 1
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = paper
			level = 2
			direction = import
			target = c:A30.market
		}
		create_trade_route = {
			goods = wood
			level = 2
			direction = import
			target = c:A30.market
		}
	}
	
	#Marrhold
	c:A29 ?= {
		create_trade_route = {
			goods = tools
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = small_arms
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = clothes
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = furniture
			level = 2
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = wine
			level = 2
			direction = import
			target = c:A28.market
		}
		create_trade_route = {
			goods = iron
			level = 2
			direction = export
			target = c:A27.market
		}
	}
	
	#Magocratic Demense
	c:A30 ?= {
		create_trade_route = {
			goods = wood
			level = 2
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = sugar
			level = 2
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = porcelain
			level = 2
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 2
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = artillery
			level = 3
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = wine
			level = 5
			direction = import
			target = c:A28.market
		}
		create_trade_route = {
			goods = paper
			level = 4
			direction = export
			target = c:A10.market
		}
	}
	
	#Silvermere
	c:A32 ?= {
		create_trade_route = {
			goods = clothes
			level = 1
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = furniture
			level = 1
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
			direction = import
			target = c:A27.market
		}
		create_trade_route = {
			goods = wine
			level = 1
			direction = import
			target = c:A28.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:A30.market
		}
		create_trade_route = {
			goods = paper
			level = 1
			direction = import
			target = c:A30.market
		}
	}

	#Trollsbay
	c:B98 ?= {
		create_trade_route = {
			goods = dye
			level = 2
			direction = import
			target = c:B27.market
		}
		create_trade_route = {
			goods = dye
			level = 1
			direction = import
			target = c:B29.market
		}
		create_trade_route = {
			goods = fabric
			level = 13
			direction = export
			target = c:A04.market
		}
		create_trade_route = {
			goods = fabric
			level = 3
			direction = export
			target = c:A01.market
		}
		create_trade_route = {
			goods = fabric
			level = 3
			direction = export
			target = c:A06.market
		}
		create_trade_route = {
			goods = fabric
			level = 3
			direction = export
			target = c:A03.market
		}
		create_trade_route = {
			goods = liquor
			level = 3
			direction = export
			target = c:A01.market
		}
		create_trade_route = {
			goods = tobacco
			level = 3
			direction = export
			target = c:A09.market
		}
		create_trade_route = {
			goods = clothes
			level = 3
			direction = export
			target = c:A09.market
		}
		create_trade_route = {
			goods = tools
			level = 3
			direction = import
			target = c:A04.market
		}
		create_trade_route = {
			goods = small_arms
			level = 2
			direction = import
			target = c:B05.market
		}
		create_trade_route = {
			goods = wine
			level = 1
			direction = export
			target = c:A02.market
		}
	}

	
	#Sarda
	c:B29 ?= {
		create_trade_route = {
			goods = grain
			level = 3
			target =c:B98.market
			direction = export
		}
		create_trade_route = {
			goods = dye
			level = 1
			target =c:B98.market
			direction = export
		}
		create_trade_route = {
			goods = sugar
			level = 2
			target =c:B98.market
			direction = import
		}
		create_trade_route = {
			goods = iron
			level = 1
			target = c:B36.market
			direction = import
		}
	}

	#Argezvale
	c:B36 ?= {
		create_trade_route = {
			goods = iron
			level = 1
			target =c:B29.market
			direction = export
		}
	}

	#Vels Fadhecai
	c:B45 ?= {
		create_trade_route = {
			goods = paper
			level = 2
			target = c:B29.market
			direction = export
		}
	}

	#Elathael
	c:B37 ?= {
		create_trade_route = {
			goods = small_arms
			level = 1
			target = C:B30.market
			direction = import
		}
	}

	#Vanbury Guild
	c:B05 ?= {
		create_trade_route = {
			goods = iron
			level = 3
			direction = export
			target = c:B85.market
		}
		create_trade_route = {
			goods = iron
			level = 2
			direction = export
			target = c:B84.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = export
			target = c:B85.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = export
			target = c:B84.market
		}
		create_trade_route = {
			goods = lead
			level = 3
			direction = import
			target = c:B84.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 1
			direction = import
			target = c:A03.market
		}
	}

	#Arakeprun
	c:B84 ?= {
		create_trade_route = {
			goods = luxury_clothes
			level = 2
			direction = export
			target = c:B85.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 2
			direction = export
			target = c:B05.market
		}
		create_trade_route = {
			goods = tools
			level = 3
			direction = import
			target = c:B85.market
		}
	}

	#Bosancovac - main Ynnic trade hub
	c:B39 ?= {
		create_trade_route = {
			goods = tools
			level = 2
            direction = import
			target = c:B49.market
		}
		create_trade_route = {
			goods = fabric
			level = 2
            direction = import
			target = c:B49.market
		}
		create_trade_route = {
			goods = fabric
			level = 2
            direction = import
			target = c:B29.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
            direction = export
			target = c:B49.market
		}
		create_trade_route = {
			goods = luxury_furniture
			level = 1
            direction = export
			target = c:B29.market
		}
		create_trade_route = {
			goods = wood
			level = 2
            direction = import
			target = c:B52.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
            direction = export
			target = c:B29.market
		}
	}

	#Tiru Moine
	c:B42 ?= {
		create_trade_route = {
			goods = tools
			level = 1
			target = c:B98.market
			direction = export
		}
		create_trade_route = {
			goods = liquor
			level = 1
			target = c:B41.market
			direction = export
		}
		create_trade_route = {
			goods = sugar
			level = 1
            direction = import
			target = c:B98.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
            direction = import
			target = c:B98.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
            direction = import
			target = c:B98.market
		}
	}

	#Plumstead
	c:B41 ?= {
		create_trade_route = {
			goods = paper
			level = 1
			target = c:B42.market
			direction = export
		}
		create_trade_route = {
			goods = iron
			level = 1
			target = c:B98.market
			direction = import
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			target = c:B98.market
			direction = import
		}
		create_trade_route = {
			goods = sugar
			level = 2
			target = c:B42.market
			direction = export
		}
	}

	#Triarchy
	c:B07 ?= {
		create_trade_route = {
			goods = damestear
			level = 1
			target = c:C02.market
			direction = import
		}
		create_trade_route = {
			goods = steel
			level = 5
			target = c:B05.market
			direction = import
		}
		create_trade_route = {
			goods = small_arms
			level = 3
			target = c:B05.market
			direction = import
		}
        create_trade_route = {
            goods = manowars
            level = 4
            target = c:B05.market
            direction = import
        }
        create_trade_route = {
            goods = artillery
            level = 3
            target = c:B05.market
            direction = import
        }
        create_trade_route = {
            goods = luxury_clothes
            level = 1
            target = c:A03.market
            direction = import
        }
        create_trade_route = {
            goods = artificery_doodads
            level = 1
            target = c:B05.market
            direction = export
        }
		create_trade_route = {
			goods = clothes
			level = 2
			target = c:B05.market
			direction = export
		}
		create_trade_route = {
			goods = liquor
			level = 5
			target = c:A01.market
			direction = export
		}
		create_trade_route = {
			goods = grain
			level = 4
			target = c:Y03.market
			direction = import
		}
	}

	#Jaddanzar
	c:F01 ?= {
		create_trade_route = {
			goods = small_arms
			level = 3
			direction = export
			target = c:R04.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 2
			direction = export
			target = c:R04.market
		}
		create_trade_route = {
			goods = luxury_clothes
			level = 1
			direction = export
			target = c:F13.market
		}
		create_trade_route = {
			goods = grain
			level = 8
			direction = export
			target = c:F14.market
		}
	}

	#Overclan
	c:F14 ?= {
		create_trade_route = {
			goods = artillery
			level = 1
			direction = import
			target = c:A09.market
		}
        create_trade_route = {
			goods = tea
			level = 3
			direction = export
			target = c:D18.market
		}
        create_trade_route = {
			goods = tea
			level = 2
			direction = export
			target = c:A80.market
		}
        create_trade_route = {
			goods = tea
			level = 2
			direction = export
			target = c:F02.market
		}
	}

	#Surakesi League
	c:F02 ?= {
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:A09.market
		}
        create_trade_route = {
			goods = iron
			level = 2
			direction = import
			target = c:F14.market
		}
        create_trade_route = {
			goods = wood
			level = 3
			direction = import
			target = c:F14.market
		}
	}

	#Old Command
	c:R08 ?= {
		create_trade_route = {
			goods = silk
			level = 1
			direction = import
			target = c:Y09.market
		}
	}

	#Yodashikyu
	c:R13 ?= {
		create_trade_route = {
			goods = silk
			level = 6
			direction = export
			target = c:R07.market
		}
		create_trade_route = {
			goods = paper
			level = 4
			direction = export
			target = c:R72.market
		}
		create_trade_route = {
			goods = tools
			level = 1
			direction = import
			target = c:R19.market
		}
	}

	#Dhugajir
	c:R72 ?= {
		create_trade_route = {
			goods = meat
			level = 5
			direction = export
			target = c:R11.market
		}
		create_trade_route = {
			goods = meat
			level = 2
			direction = export
			target = c:R10.market
		}
	}
	
	#Tianlou
	c:Y03 ?= {
		create_trade_route = {
			goods = paper
			level = 6
			direction = export
			target = c:A06.market
		}
		create_trade_route = {
			goods = wood
			level = 10
			direction = import
			target = c:Y11.market
		}
		create_trade_route = {
			goods = wood
			level = 9
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = fabric
			level = 6
			direction = import
			target = c:Y11.market
		}
		create_trade_route = {
			goods = artillery
			level = 3
			direction = import
			target = c:A06.market
		}
		create_trade_route = {
			goods = tea
			level = 2
			direction = import
			target = c:Y08.market
		}
		create_trade_route = {
			goods = tea
			level = 3
			direction = export
			target = c:A06.market
		}
		create_trade_route = {
			goods = dye
			level = 8
			direction = import
			target = c:A06.market
		}
	}

	#Zhiqian
	c:Y08 ?= {
		create_trade_route = {
			goods = paper
			level = 4
			direction = import
			target = c:Y03.market
		}
		create_trade_route = {
			goods = tea
			level = 4
			direction = export
			target = c:A06.market
		}
	}

	#Shuvuushdi
	c:Y11 ?= {
		create_trade_route = {
			goods = paper
			level = 6
			direction = import
			target = c:Y03.market
		}
	}
	
	#Ozovar
	c:Y20 ?= {
		create_trade_route = {
			goods = silk
			level = 2
			direction = import
			target = c:Y17.market
		}
        create_trade_route = {
			goods = artillery
			level = 1
			direction = export
			target = c:Y17.market
		}
        create_trade_route = {
			goods = artillery
			level = 1
			direction = export
			target = c:Y19.market
		}
	}
	
	#Hakuukulga
	c:Y41 ?= {
		create_trade_route = {
			goods = tools
			level = 1
			direction = import
			target = c:Y43.market
		}
		create_trade_route = {
			goods = wood
			level = 1
			direction = export
			target = c:Y43.market
		}
	}
	
	#YBazuneizar
	c:Y43 ?= {
		create_trade_route = {
			goods = tools
			level = 1
			direction = export
			target = c:Y41.market
		}
		create_trade_route = {
			goods = wood
			level = 1
			direction = import
			target = c:Y41.market
		}
	}
	
	c:C30 ?= { #Kheios
		create_trade_route = {
			goods = manowars
			level = 1 
			direction = export
			target = c:C33.market
		}
		create_trade_route = {
			goods = manowars
			level = 1 
			direction = export
			target = c:C35.market
		}
		create_trade_route = {
			goods = manowars
			level = 1 
			direction = export
			target = c:C31.market
		}
		create_trade_route = {
			goods = manowars
			level = 1 
			direction = export
			target = c:C36.market
		}
	}
	c:C31 ?= { #Anisikheion
		create_trade_route = {
			goods = fabric
			level = 1 
			direction = import
			target = c:C33.market
		}
		create_trade_route = {
			goods = oil
			level = 1 
			direction = export
			target = c:C39.market
		}
		create_trade_route = {
			goods = clippers
			level = 1 
			direction = export
			target = c:C34.market
		}
		create_trade_route = {
			goods = clippers
			level = 1 
			direction = export
			target = c:C30.market
		}
		create_trade_route = {
			goods = fish
			level = 1 
			direction = export
			target = c:C39.market
		}
		create_trade_route = {
			goods = tools
			level = 1
			direction = import
			target= c:C39.market
		}
	}
	C:C33 ?= { #Keyolion
		create_trade_route = {
			goods = liquor
			level = 1
			direction = export
			target = c:C30.market
		}
		create_trade_route = {
			goods = liquor
			level = 1 
			direction = export
			target = c:C34.market
		}
		create_trade_route = {
			goods = liquor
			level = 1 
			direction = export
			target = c:C38.market
		}
		create_trade_route = {
			goods = dye
			level = 1 
			direction = import
			target = c:C39.market
		}
		create_trade_route = {
			goods = porcelain
			level = 1
            direction = export
			target = c:C39.market
		}
	}
	c:C34 ?= { #Eneion
		create_trade_route = {
			goods = fabric
			level = 1
			direction = import
			target = c:C36.market
		}
		create_trade_route = {
			goods = manowars
			level = 1
			direction = import
			target = c:B05.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:B05.market
		}
		create_trade_route = {
			goods = tools
			level = 1
			direction = export
			target = c:C35.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
			direction = export
			target = c:C35.market
		}
		create_trade_route = {
			goods = tools
			level = 1
			direction = export
			target = c:C70.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
			direction = export
			target = c:C31.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
			direction = export
			target = c:C70.market
		}
	}
	c:C35 ?= { #Besolaki
		create_trade_route = {
			goods = furniture
			level = 1
			direction = export
			target = c:C34.market
		}
		create_trade_route = {
			goods = furniture
			level = 1
			direction = export
			target = c:C36.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
			direction = import
			target = c:C34.market
		}
		create_trade_route = {
			goods = clippers
			level = 1
			direction = export
			target = c:C34.market
		}
		create_trade_route = {
			goods = tools
			level = 1
			direction = import
			target = c:C34.market
		}
		create_trade_route = {
			goods = tea
			level = 1
			direction = import
			target = c:C39.market
		}
	}
	c:C36 ?= { #Apikhoxi
		create_trade_route = {
			goods = fabric
			level = 1
			direction = export
			target = c:C35.market
		}
		create_trade_route = {
			goods = grain
			level = 2
			direction = export
			target = c:C30.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:C30.market
		}
		create_trade_route = {
			goods = clothes
			level = 1
			direction = export
			target = c:C33.market
		}
	}
	c:C70 ?= { #Amgremos
		create_trade_route = {
			goods = tools
			level = 2
			direction = import
			target = c:C34.market
		}
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:C30.market
		}
		create_trade_route = {
			goods = manowars
			level = 1
			direction = import
			target = c:C39.market
		}
		create_trade_route = {
			goods = wood
			level = 2
			direction = export
			target = c:C39.market
		}
		create_trade_route = {
			goods = wood
			level = 1
			direction = export
			target = c:C30.market
		}
	}
	c:C46 ?= { #Klerechend
		create_trade_route = {
			goods = small_arms
			level = 1
			direction = import
			target = c:C39.market
		}
	}

	c:D08 ?= { #Khugdihr
		create_trade_route = {
			goods = tools
			level = 7
			direction = export
			target = c:A10.market
		}
		create_trade_route = {
			goods = liquor
			level = 6
			direction = export
			target = c:A10.market
		}
	}

	c:D31 ?= { #Dakaz Carzviya
		create_trade_route = {
			goods = luxury_clothes
			level = 3
			direction = import
			target = c:R07.market
		}
	}

    c:B01 ?= { #Asraport
        create_trade_route = {
			goods = iron
			level = 1
			direction = import
			target = c:A03.market
		}
        create_trade_route = {
			goods = fabric
			level = 1
			direction = import
			target = c:A03.market
		}
    }
}
