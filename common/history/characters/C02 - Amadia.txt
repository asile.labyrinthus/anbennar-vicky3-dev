﻿CHARACTERS = {
	c:C02 ?= {
		create_character = {
			first_name = "Varilor"
			last_name = "Silmuna"
			historical = yes
			ruler = yes
			age = 42
			birth_date = 1787.9.26
			ideology = ideology_royalist	#probably magocratic?
			dna = dna_varilor_silmuna
			traits = {
				ambitious celebrity_commander
			}
		}
	}
}
