﻿CHARACTERS = {
	c:B91 ?= {
		create_character = { #Rogier Lencord is a ranger of Ebenmas, whose family originates from escanni and lecori horsemen alike. Lead the coup which united the dustmen and is now leading a war against elathael, seeking to unite the expanse.’
			first_name = Rogier
			last_name = Lencord
			historical = yes
			age = 34
			ruler = yes
			interest_group = ig_armed_forces
			ig_leader = yes
			commander = yes
			ideology = ideology_authoritarian
			traits = {
				honorable brave scarred basic_colonial_administrator  
			}
		}

		create_character = { # firearms tycoon who supplied the Ranger coup with high-quality bucks guns. Is wary about ynnic interference in the expanse and wants them out of ynnsman affairs.
			first_name = Varil 
			last_name = Barnes
			historical = yes
			age = 49
			interest_group = ig_industrialists 
			ig_leader = yes
			ideology = ideology_jingoist 
			traits = {
				cautious cruel engineer
			}
		}
	}
}
