﻿CHARACTERS = {
	c:F01 ?= {
		create_character = {
			first_name = "Kamnaril"
			last_name = "Andrellzuir"
			historical = yes
			age = 322
			culture = cu:sun_elven
			interest_group = ig_armed_forces
			ruler = yes
			ideology = ideology_jingoist_leader
			traits = {
				charismatic
				ambitious
				experienced_political_operator
				plains_commander
			}
		}
	}
}
