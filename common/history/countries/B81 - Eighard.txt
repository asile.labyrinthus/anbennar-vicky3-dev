﻿COUNTRIES = {
	c:B81 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = lathe
		add_technology_researched = tradition_of_equality

		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_women_in_the_workplace
	}
}