﻿COUNTRIES = {
	c:C06 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_pragmatic_application
		activate_law = law_type:law_pragmatic_artifice
		activate_law = law_type:law_traditional_magic_encouraged
	}
}