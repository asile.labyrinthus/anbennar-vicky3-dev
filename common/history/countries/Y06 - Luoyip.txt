﻿COUNTRIES = {
	c:Y06 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = urban_planning
		add_technology_researched = sericulture
		add_technology_researched = academia
		add_technology_researched = law_enforcement
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army

		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_migration_controls
	}
}