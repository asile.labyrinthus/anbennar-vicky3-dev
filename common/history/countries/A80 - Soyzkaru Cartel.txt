﻿COUNTRIES = {
	c:A80 ?= {
		effect_starting_technology_tier_3_tech = yes
		effect_starting_artificery_tier_2_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_secret_police

		activate_law = law_type:law_laissez_faire # INVALID - does not have the tech
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_pragmatic_application
		activate_law = law_type:law_pragmatic_artifice
		activate_law = law_type:law_artifice_encouraged
	}
}