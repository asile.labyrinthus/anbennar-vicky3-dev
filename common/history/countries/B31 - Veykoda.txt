﻿COUNTRIES = {
	c:B31 ?= {
		effect_starting_technology_tier_4_tech = yes

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_right_of_assembly #just not enough central control unlike in Sarda
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_tenant_farmers #no serfdom unlike Ynnics

		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_traditional_magic_only
		activate_law = law_type:law_pragmatic_artifice #Bloodrunes
	}
}