﻿POPS = {
	s:STATE_IOCHAND = {
		region_state:A15 = {
			create_pop = {
				culture = creek_gnome
				size = 221809
			}
			create_pop = {
				culture = reverian
				size = 17706
			}
		}
		region_state:A07 = {
			create_pop = {
				culture = reverian
				size = 890320
			}
		}
	}

	s:STATE_REVERIA = {
		region_state:A07 = {
			create_pop = {
				culture = reverian
				size = 1512744
			}
		}
	}

	s:STATE_GNOMISH_PASS = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 238562
			}
			create_pop = {
				culture = reverian
				size = 54671
			}
		}
	}

	s:STATE_THE_IONDDAMMO = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 504699
			}
			create_pop = {
				culture = creek_gnome
				size = 173602
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 428414
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.75
						kobold_dragon_cult = 0.25
					}
				}
			}
		}
	}

	s:STATE_ODDANROY = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 484419
			}
			create_pop = {
				culture = creek_gnome
				size = 280798
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 547414
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.75
						kobold_dragon_cult = 0.25
					}
				}
			}
		}
	}
	
	s:STATE_DRAGONSPINE = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 16857
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 377160
			}
		}
	}

	s:STATE_STORM_ISLES = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 28560
			}
		}
	}

	s:STATE_BRECC = {
		region_state:A17 = {
			create_pop = {
				culture = cliff_gnome
				size = 420425
			}
			create_pop = {
				culture = creek_gnome
				size = 35700
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 705600
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.5
						kobold_dragon_cult = 0.5
					}
				}
			}
		}
	}

	s:STATE_KOBILDZAN = {
		region_state:A16 = {
			create_pop = {
				culture = kobildzani_kobold
				size = 5464206	#10 million boys, just for fun
			}
		}
	}
	s:STATE_KOBILDZAN_2 = {
		region_state:A16 = {
			create_pop = {
				culture = kobildzani_kobold
				size = 5464206	#10 million boys, just for fun
			}
		}
	}
	
	s:STATE_LONELY_ISLE = {
		region_state:A03 = {
			create_pop = {
				culture = derannic
				size = 13856
			}
		}
	}
}
