﻿ig_trait_bastion_of_democracy = {
	icon = "gfx/interface/icons/ig_trait_icons/propagandists.dds"
	min_approval = loyal
	
	modifier = {
		country_infamy_decay_mult = 0.1
	}
}