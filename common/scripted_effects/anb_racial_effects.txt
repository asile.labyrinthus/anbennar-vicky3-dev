﻿setup_racial_flags = {
	every_scope_culture = {
		limit = { NOT = { has_variable = race_id_set } }
		set_variable = race_id_set
		set_racial_flag = yes
	}
}

set_racial_flag = {
	if = {
		limit = { has_discrimination_trait = centaur_race_heritage }
		set_variable = {
			name = race_id
			value = 1
		}
	}
	else_if = {
		limit = { has_discrimination_trait = dwarven_race_heritage }
		set_variable = {
			name = race_id
			value = 2
		}
	}
	else_if = {
		limit = { has_discrimination_trait = elven_race_heritage }
		set_variable = {
			name = race_id
			value = 3
		}
	}
	else_if = {
		limit = { has_discrimination_trait = gnollish_race_heritage }
		set_variable = {
			name = race_id
			value = 4
		}
	}
	else_if = {
		limit = { has_discrimination_trait = gnomish_race_heritage }
		set_variable = {
			name = race_id
			value = 5
		}
	}
	else_if = {
		limit = { has_discrimination_trait = goblin_race_heritage }
		set_variable = {
			name = race_id
			value = 6
		}
	}
	else_if = {
		limit = { has_discrimination_trait = halfling_race_heritage }
		set_variable = {
			name = race_id
			value = 7
		}
	}
	else_if = {
		limit = { has_discrimination_trait = half_elven_race_heritage }
		set_variable = {
			name = race_id
			value = 8
		}
	}
	else_if = {
		limit = { has_discrimination_trait = half_orcish_race_heritage }
		set_variable = {
			name = race_id
			value = 9
		}
	}
	else_if = {
		limit = { has_discrimination_trait = harimari_race_heritage }
		set_variable = {
			name = race_id
			value = 10
		}
	}
	else_if = {
		limit = { has_discrimination_trait = harpy_race_heritage }
		set_variable = {
			name = race_id
			value = 11
		}
	}
	else_if = {
		limit = { has_discrimination_trait = hobgoblin_race_heritage }
		set_variable = {
			name = race_id
			value = 12
		}
	}
	else_if = {
		limit = { has_discrimination_trait = kobold_race_heritage }
		set_variable = {
			name = race_id
			value = 14
		}
	}
	else_if = {
		limit = { has_discrimination_trait = lizardman_race_heritage }
		set_variable = {
			name = race_id
			value = 15
		}
	}
	else_if = {
		limit = { has_discrimination_trait = mechanim_race_heritage }
		set_variable = {
			name = race_id
			value = 16
		}
	}
	else_if = {
		limit = { has_discrimination_trait = ogre_race_heritage }
		set_variable = {
			name = race_id
			value = 17
		}
	}
	else_if = {
		limit = { has_discrimination_trait = orcish_race_heritage }
		set_variable = {
			name = race_id
			value = 18
		}
	}
	else_if = {
		limit = { has_discrimination_trait = aelantiri_heritage }
		set_variable = {
			name = race_id
			value = 19
		}
	}
	else_if = {
		limit = { has_discrimination_trait = troll_race_heritage }
		set_variable = {
			name = race_id
			value = 20
		}
	}
	else_if = {
		limit = {
			OR = {
				has_discrimination_trait = cannorian_heritage
				has_discrimination_trait = bulwari_heritage
				has_discrimination_trait = east_sarhaly_heritage
				has_discrimination_trait = southeast_halessi_heritage
				has_discrimination_trait = northeast_halessi_heritage
				has_discrimination_trait = west_sarhaly_heritage
				has_discrimination_trait = south_sarhaly_heritage
				has_discrimination_trait = east_sarhaly
				has_discrimination_trait = triunic
			}
		}

		set_variable = {
			name = race_id
			value = 13
		}
	}
	else = {
		debug_log = "Could not find race for culture, setting to none"

		set_variable = {
			name = race_id
			value = 0
		}
	}
}
